# worldsailing SoticMembers 

### PHP library description
Database interface bundle of sotic_members database for World Sailing microservices based on Silex.

  
### Install by composer
````php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://worldsailing@bitbucket.org/worldsailing/soticmembers.git"
    }
  ],
  "require": {
    "worldsailing/soticmembers": "dev-master"
  }
}
````

### Usage
````php
use worldsailing\SimpleBiog\provider\SoticMembersProvider;

$app->register(new SoticMembersProvider($app), []);

//Then $app['service.soticMembers'] returns a SoticMembersService instance
//For example:
$model = $app['service.soticMembers']->createMembersModel($app);
````

 
### License

All right reserved
