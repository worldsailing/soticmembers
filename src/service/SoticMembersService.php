<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\SoticMembers\service;

use Silex\Application;
use worldsailing\SoticMembers\model\MembersModel;
use worldsailing\Common\BundleResultSet\CompositeListResultSet;
use worldsailing\Common\BundleResultSet\CompositeEntityResultSet;

/**
 * Class SoticMembersService
 * @package worldsailing\SoticMembers\service
 */
Class SoticMembersService
{

    /**
     * @var \Silex\Application
     */
    protected $app;

    /**
     * @param Application $app
     * @return MembersModel
     */
    public function createMembersModel(Application $app)
    {
        return new MembersModel($app);
    }

    /**
     * @param MembersModel $model
     * @param $isafId
     * @return CompositeEntityResultSet
     */
    public function getMemberByIsafId(MembersModel $model, $isafId)
    {
        //fetchAssoc
        $result = $model->getMemberByMemberLogin($isafId);
        return (new CompositeEntityResultSet($result, false));
    }

    /**
     * @param MembersModel $model
     * @param $email
     * @return CompositeListResultSet
     */
    public function getMembersByEmail(MembersModel $model, $email)
    {
        //fetchAll
        $result = $model->getMembersByMemberEmail($email);
        return (new CompositeListResultSet($result, false));
    }

    /**
     * @param MembersModel $model
     * @param $isafIds
     * @return CompositeListResultSet|bool
     */
    public function getActiveMembersByIsafId(MembersModel $model, $isafIds)
    {
        //fetchAll
        if (is_array($isafIds) && (count($isafIds) > 0)) {
            $result = $model->getActiveMembersByMemberLogin($isafIds);
        } else {
            $result = false;
        }
        return (new CompositeListResultSet($result, false));
    }


}