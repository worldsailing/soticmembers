<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\SoticMembers\model;

/**
 * Class MembersModel
 * @package worldsailing\SoticMembers\model
 */
class MembersModel extends AbstractModel
{

    /**
     * MembersModel constructor.
     * @param \Silex\Application $app
     */
    public function __construct($app)
    {
        parent::__construct($app);

    }

    /**
     * @param string $membLogin
     * @return array|bool
     */
    public function getMemberByMemberLogin($membLogin = '')
    {
        $sql = "
            SELECT * FROM members WHERE MembProjId = 'isaf' 
                AND MembLogin = ?
        ";
        $result =  $this->app['dbs']['sotic_members']->fetchAssoc($sql, array((string) $membLogin));
        return ($result) ? $result : false;
    }

    /**
     * @param string $email
     * @return array|bool
     */
    public function getMembersByMemberEmail($email = '')
    {
        $sql = "
            SELECT * FROM members WHERE MembProjId = 'isaf' 
                AND MembEmail = ?
        ";
        $result =  $this->app['dbs']['sotic_members']->fetchAll($sql, array((string) $email));
        return ($result && is_array($result) && count($result) > 0) ? $result : false;
    }

    /**
     * @param array $membLogins
     * @return array|bool
     */
    public function getActiveMembersByMemberLogin($membLogins = [])
    {
        $sql = "
            SELECT * From members WHERE MembProjId = 'isaf' 
                AND MembActive IN ('New','Yes','Resend')
                AND MembLogin IN (?)  
        ";
        $result = $this->app['dbs']['sotic_members']->fetchAll($sql, array($membLogins), array(\Doctrine\DBAL\Connection::PARAM_INT_ARRAY) );
        return ($result && is_array($result) && count($result) > 0) ? $result : false;
    }


}
