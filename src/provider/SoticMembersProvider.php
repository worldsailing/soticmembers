<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\SoticMembers\provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use worldsailing\SoticMembers\service\SoticMembersService;

/**
 * Class SoticMembersProvider
 * @package worldsailing\SoticMembers\provider
 */
class SoticMembersProvider implements ServiceProviderInterface
{

    /**
     * @param Container $app
     */
    public function register(Container $app)
    {

        $app['service.soticMembers'] = function ($app) {
            return new SoticMembersService($app);
        };

        return;
    }
}
